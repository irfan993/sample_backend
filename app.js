const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const fs = require('fs');
const https = require('https');
const cors = require('cors')

const expressValidator = require('express-validator');
const customValidator = require('./application/common/customvalidator');
require('./config/mongo.config');

const app = express();

app.use(express.static(path.join(__dirname, 'uploads')));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');
app.set('view engine', 'ejs');

app.use(cors());

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(expressValidator(customValidator.middlewareObj));

app.use('/api/', require('./routes/index.route'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {

    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    res.json(err.message).status(err.status || 500);

});


//module.exports = app;
const port = process.env.PORT || 3000;

const server = app.listen(port, function(){

    console.log('listening to port '+port+' dealer');
});

/* this code connects to socket and global._io is global variable
and can be used to listen and emit events in any controller and repository*/
global._io = require('socket.io').listen(server);

_io.on('connection', function(socket){

    console.log("connected");
    _io.emit( 'test_Socket' , { test : "socket connected" });
});


