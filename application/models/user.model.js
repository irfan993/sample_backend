const mongoose = require('mongoose');
const constants = require('../common/constants');
const bcrypt = require('bcrypt');


var userSchema = mongoose.Schema({

    first_name : {
        type : String,
        required : true
    },
    last_name: {
        type : String,
        required : true
    },
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    username : {
        type: String,
        required: true
    },
    is_user_active:{
        type:Number,
        default : 1, // 0->inactive //1->active
    },
    modified_at : {
        type: Date,
        default : Date.now
    },
    created_at : {
        type: Date,
        default : Date.now
    },
});

userSchema.pre('save', function (next) {
    let user = this;
    if (this.isModified('password') || this.isNew) {

        bcrypt.genSalt(10, function (err, salt) {
            if (err) {

                return next(err);
            }
            bcrypt.hash(user.password, salt, function (err, hash) {
                if (err) {
                    return next(err);
                }
                user.password = hash;
                next();
            });
        });
    } else {
        return next();
    }
});

userSchema.methods.comparePassword = function (password) {

    return bcrypt.compare(password, this.password);
};

userSchema.path('email').validate(function (value, respond) {
    return mongoose.model('User').count({ email: value }).exec().then(function (count) {
        return !count;
    })
    .catch(function (err) {
        throw err;
    });
}, 'Email already exists.');


module.exports = mongoose.model('User', userSchema);