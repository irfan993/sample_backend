const userRepository = require('../repositories/user.repository');
const customHelper = require('../common/customhelper');
const constants = require('../common/constants');
const httpStatus = require('http-status');
const customValidator = require('../common/customvalidator');


module.exports.getUser = (req , res, next) => {



     userRepository.getUser(req)
        .then( (data) => {

            customHelper.sendJsonResponse(res, httpStatus.OK, data, constants.success_message.success_get_msg);
            return;
        }).catch( (err) => {

        customHelper.sendJsonError(res, err);
        return;
    })
};


module.exports.delete = (req , res, next) => {

    userRepository.delete(req)
        .then( (data) => {

            customHelper.sendJsonResponse(res, httpStatus.OK, data, constants.success_message.success_get_msg);
            return;
        }).catch( (err) => {

            customHelper.sendJsonError(res, err);
            return;
        })
};