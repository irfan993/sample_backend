const commonConfig = require('../../config/common.config');
const constants = require('../common/constants');
const httpStatus = require('http-status');
const dotenv = require('dotenv').config();
const CustomHelper = {};
CustomHelper.sendJsonResponse = function (res, status, data, msg) {

    let response;
    response = {
        success: true,
    };
    if (data)
        response['data'] = data;

    if (msg)
        response['message'] = msg;
    res.status(status).json(response);
    return;
};

CustomHelper.sendJsonError = function (res, err) {

    let response;
    let statusCode;
    if (err.hasOwnProperty('statusCode')) {
        statusCode = err.statusCode;
        response = {
            success: false,
            error: {
                message: err.message,
                code: err.code
            },

        }
    } else if (err.hasOwnProperty('errors')) {
        if (err.errors.hasOwnProperty('email')) {
            statusCode = httpStatus.BAD_REQUEST;
            response = {
                success: false,
                error: {
                    message: {
                        email: {
                            msg: err.errors.email.message,
                            value: err.errors.email.value
                        }
                    },
                    code: constants.error_code.email_already_exist
                },

            }
        }
    } else {
        statusCode = httpStatus.INTERNAL_SERVER_ERROR;
        response = {
            success: false,
            error: {
                message: constants.error_message.general_error,
                code: constants.error_code.general_error
            },

        };
    }
    res.status(statusCode).json(response);
    return;
};

CustomHelper.getJWTUserId = function (header) {
    return jwt.decode(header.split(' ')[1]);
};

CustomHelper.generatePassword = function (password) {
    let salt = bcrypt.genSaltSync(10);
    return bcrypt.hashSync(password, salt);
};

module.exports = CustomHelper;