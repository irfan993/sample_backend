let constants = {
    error_message : {

        general_error : "Something went wrong!",
        user_not_found : "User not found",
        invalid_credentials : "Invalid credentials",
        password_not_matched : "Password does not match",
        same_password : "Current Password and New Password cannot be same",
    },
    error_code : {

        general_error : 1000,
        validation_error : 1001,
        user_not_found : 2001,
    },
    success_message : {

        registered_successfully : "Registered successfully, now you can login",
        logged_in_successfully : "Logged in successfully",
    }
};

module.exports = constants;