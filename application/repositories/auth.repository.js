const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const constants = require('../common/constants');
const jwtConfig = require('../../config/jwt.config');
const bcrypt = require('bcrypt');

const customHelper = require('../common/customhelper');
const commonConfig = require('../../config/common.config');
const fs = require("fs");
require('../models/user.model');
const User = mongoose.model('User');
const httpStatus = require('http-status');
const dotenv = require('dotenv').config();

const UserRepositories = require('./user.repository');

module.exports.register = (data) => {
    
    return new Promise((resolve, reject) => {

            let newUser = new User();
            newUser.first_name = data.body.first_name;
            newUser.last_name = data.body.last_name;
            newUser.username = data.body.username;
            newUser.email = data.body.email;
            newUser.username = data.body.username;
            newUser.password = data.body.password;

            return newUser.save()
            .
        then((res_user) => {

            resolve(res_user);
            return;
        }).catch((err) => {

            reject(err);
            return;
        })
    });

};

module.exports.login = (data) => {

    return new Promise((resolve, reject) => {
        let user;

        User.findOne({email: data.body.email})
            .then(resUser => {
     
                user = resUser;
                if (!user) {
                    let errorObj = {
                        statusCode: httpStatus.BAD_REQUEST,
                        message: constants.error_message.user_not_found,
                        code: constants.error_code.user_not_found
                    };
                    reject(errorObj);
                    return;
                }
                return resUser.comparePassword(data.body.password)
            })
            .then(isMatch => {

                if (isMatch) {
                    if (user.is_blocked === true) {
                        let errorObj = {
                            statusCode: httpStatus.UNAUTHORIZED,
                            message: constants.error_message.user_is_blocked,
                            code: constants.error_code.user_is_blocked
                        };
                        reject(errorObj);
                        return;
                    }
                    if (user.is_user_active === 0) {
                        let errorObj = {
                            statusCode: httpStatus.UNAUTHORIZED,
                            message: constants.error_message.activation_failed,
                            code: constants.error_code.activation_failed
                        };
                        reject(errorObj);
                        return;
     
                    }

                    let token = jwt.sign({user: user}, jwtConfig.secret);
      
                    resolve({token: token, user: user});
                    return;

                } else {
                    let errorObj = {
                        statusCode: httpStatus.UNAUTHORIZED,
                        message: constants.error_message.invalid_credentials,
                        code: constants.error_code.invalid_credentials
                    };
                    reject(errorObj);
                    return;
                }
            })
            .catch((err) => {
            reject(err);
            return;
        })
    });
};

