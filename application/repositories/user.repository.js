const UserRepositories = require('./user.repository');
const httpStatus = require('http-status');
const constants = require('../common/constants');

const User = require('./../models/user.model');
module.exports.getUser = (req) => {

    return new Promise( (resolve, reject) => {

        User.find({})
        .then( (res) => {

            resolve(res);
            return;
        }).catch( (err) => {

            reject(err);
            return;
        });
    });
};

module.exports.delete = (req) => {

    return new Promise( (resolve, reject) => {

        User.findOne({ _id : req.body.id}).remove()
        .then( (res) => {

            resolve(res);
            return;
        }).catch( (err) => {

            reject(err);
            return;
        });
    });
};