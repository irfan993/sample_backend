const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();
const authController  = require('../application/controllers/auth.controller');
const UserController  = require('../application/controllers/user.controller');
const customHelper = require('../application/common/customhelper');
const user = require('../application/models/user.model');
const authMiddleware = require('../application/middlewares/auth.middleware');
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/register',authController.register);
router.post('/login',authController.login);
router.get('/users', authMiddleware, UserController.getUser);
router.post('/delete', authMiddleware, UserController.delete);
module.exports = router;
